const link = 'mongodb://localhost:27017/taskmanager'
const PORT = process.env.PORT || 3000
const url = `http://localhost:${PORT}`

module.exports = {
    link, PORT, url
}