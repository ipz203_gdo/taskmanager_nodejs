const express = require('express')
const User = require('../models/user')
const router = new express.Router()


// Виведено користувачів
router.get('/users', async (request, response) => {
   await User.find({}).then((users) => {
        response.status(200).send(users)
        console.log("Виведено користувачів")
   }).catch((error) => {
       response.status(404).send(error)
   })
})


//Виведено користувача за ID
router.get('/users/:id', async (request, response) => {
    let id = request.params.id
    await User.findById(id).then((users) => {
        response.status(200).send(users)
        console.log("Виведено користувача за ID")
    }).catch((error) => {
        response.status(404).send(error.message)
    })
})


//Додано користувача
router.post('/users', async (request, response) => {
    try{
        let user = new User(request.body)
        await user.save()
        response.status(200).send(user)
        console.log("Додано користувача")
    }catch (error){
        response.status(500).send(error.message)
    }
})

//Видалено користувача
router.get('/user/:id', async (request, response) => {
    let id = request.params.id
    try{
        await User.findByIdAndDelete(id)
        response.status(200).send('Видалено успішно')
        console.log("Видалено користувача")
    }catch (error){
        response.status(404).send(error.message)
    }
})

module.exports = router
