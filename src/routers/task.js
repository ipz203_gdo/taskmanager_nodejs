const express = require('express')
const req = require('express/lib/request')
const Task = require('../models/task')
const router = new express.Router()

//Виведення завдань
router.get('/tasks', async (request, response) => {
   await Task.find({}).then((tasks) => {
        response.status(200).send(tasks)
        console.log("Виведення завдань")
   }).catch((error) => {
       response.status(404).send(error)
   })
})
//Виведено завдання за ID
router.get('/tasks/:id', async (request, response) => {
    let id = request.params.id
    await Task.findById(id).then((tasks) => {
        response.status(200).send(tasks)
        console.log("Виведення завдання за ID")
    }).catch((error) => {
        response.status(404).send(error.message)
    })
})

//Додано завдання
router.post('/tasks', async (request, response) => {
    try{
        let task = new Task(request.body)
        await task.save()
        response.status(200).send(task)
        console.log("Додано завдання")
    }catch (error){
        response.status(500).send(error.message)
    }
})

//Видалено завдання за ID
router.get('/task/:id', async (request, response) => {
    let id = request.params.id
    try{
        await Task.findByIdAndDelete(id)
        response.status(200).send('Видалено успішно')
        console.log("Видалено завдання за ID")
    }catch (error){
        response.status(404).send(error.message)
    }
})

module.exports = router