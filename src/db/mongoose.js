const mongoose = require('mongoose')
const {link} = require('../config/index')

const connection = async() => {
    try {
        await mongoose.connect(link)
    }
    catch (e) {
        throw new Error(e)
    }
}

module.exports = connection