const mongoose = require('mongoose')
const express = require('express')
const fs = require('fs')
const connDB = require('./db/mongoose')

const { PORT } = require('./config/index')

//Підключення всіх файлів у папці models з .js
fs.readdirSync(__dirname + '/models').forEach(function(filename) {
    if (~filename.indexOf('.js')) require(__dirname + '/models/' + filename)
  });


const usersRouter = require('./routers/user')
const tasksRouter = require('./routers/task')

const app = express()

app.use(express.json())
app.use(usersRouter)
app.use(tasksRouter)

connDB().then(() => {
    app.listen(PORT, () => {
        console.log(`Listening on port ${PORT}`)
    })
    
}).catch((err) => {
    console.log(err.message)
})