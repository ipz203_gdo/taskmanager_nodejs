const mongoose = require('mongoose')
const validator = require('validator')

//Створення схеми
const UserSchema = mongoose.Schema({
    name:
        {
            type: String,
            required: true,
            trim: true,
        },
    age:
        {
            type: Number,
            default: 0,
            validate(value){
                if(value < 0){
                    throw new Error("Вік має бути додатнім числом")
                }
            }
        },
    email:
        {
            type: String,
            unique: true,
            lowercase: true,
            validate(value){
                if(!validator.isEmail(value)){
                    throw new Error("Невірна пошта")
                }
            }
        },
    password:
        {
            type: String,
            required: true,
            min: 7,
            trim: true,
            validate(value){
                if(validator.contains(value,"password")){
                    throw new Error("Пароль не може містити слово password'")
                }
            }
        }
})

//Створення моделі схеми
const User = mongoose.model('User', UserSchema)

module.exports = User


