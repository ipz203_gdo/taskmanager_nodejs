const mongoose = require('mongoose')
const validator = require('validator')

//Створення схеми
const TaskSchema = mongoose.Schema({
    description:
        {
            type: String,
            required: true,
            trim: true
        },
    completed:
        {
            type: Boolean,
            default: false,
            required: false
        }
})

//Створення моделі схеми
const Task = mongoose.model('Task', TaskSchema)

module.exports = Task